package com.cengha.bkry.dao;

import com.cengha.bkry.domain.PrePack;
import com.cengha.bkry.domain.Product;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

@Component
public class ProductDao {

    private Map<String, Product> products;


    public ProductDao() {
        products = new HashMap<>();

        List<PrePack> prePacks1 = new ArrayList<>();
        prePacks1.add(PrePack.builder().quantity(3).price(BigDecimal.valueOf(6.99)).build());
        prePacks1.add(PrePack.builder().quantity(5).price(BigDecimal.valueOf(8.99)).build());

        List<PrePack> prePacks2 = new ArrayList<>();
        prePacks2.add(PrePack.builder().quantity(2).price(BigDecimal.valueOf(9.95)).build());
        prePacks2.add(PrePack.builder().quantity(5).price(BigDecimal.valueOf(16.95)).build());
        prePacks2.add(PrePack.builder().quantity(8).price(BigDecimal.valueOf(24.95)).build());

        List<PrePack> prePacks3 = new ArrayList<>();
        prePacks3.add(PrePack.builder().quantity(3).price(BigDecimal.valueOf(5.95)).build());
        prePacks3.add(PrePack.builder().quantity(5).price(BigDecimal.valueOf(9.95)).build());
        prePacks3.add(PrePack.builder().quantity(9).price(BigDecimal.valueOf(16.99)).build());

        products.put("VS5", Product.builder().code("VS5").name("Vegemite Scroll").prePacks(prePacks1).build());
        products.put("MB11", Product.builder().code("MB11").name("Blueberry Muffin").prePacks(prePacks2).build());
        products.put("CF", Product.builder().code("CF").name("Croissant").prePacks(prePacks3).build());


    }

    public Optional<Product> getByCode(String selection) {
        return Optional.of(products.get(selection));
    }
}
