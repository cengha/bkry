package com.cengha.bkry.service;

import com.cengha.bkry.dao.ProductDao;
import com.cengha.bkry.domain.Order;
import com.cengha.bkry.domain.Pack;
import com.cengha.bkry.domain.Product;
import com.cengha.bkry.domain.dto.OrderPostRequest;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private ProductDao productDao;
    private PackService packService;

    OrderService(ProductDao productDao, PackService packService) {
        this.productDao = productDao;
        this.packService = packService;
    }

    public List<Order> create(List<OrderPostRequest> items) {
        return items.stream()
                .map(o -> this.create(o.getQuantity(), o.getCode()))
                .collect(Collectors.toList());
    }

    private Order create(Integer quantity, String code) {

        Product product = productDao.getByCode(code).orElseThrow(RuntimeException::new);

        List<Pack> packs = packService.pack(quantity, product).stream()
                .filter(p -> p.getCount() != 0)
                .collect(Collectors.toList());

        return this.map(quantity, packs, product);
    }

    private Order map(Integer quantity, List<Pack> packs, Product product) {
        List<String> items = new ArrayList<>();
        BigDecimal sum = BigDecimal.ZERO;
        for (Pack pack : packs) {
            sum = sum.add(pack.getPrePack().getPrice().multiply(BigDecimal.valueOf(pack.getCount())));
            items.add(pack.getCount() + " x " + pack.getPrePack().getQuantity() + " $" + pack.getPrePack().getPrice());
        }
        String summary = quantity + " " + product.getCode() + " $" + sum.stripTrailingZeros();
        return Order.builder().items(items).summary(summary).build();
    }
}
