package com.cengha.bkry.service;

import com.cengha.bkry.domain.Pack;
import com.cengha.bkry.domain.PrePack;
import com.cengha.bkry.domain.Product;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
class PackService {

    //TODO:this method should be recursive and so independent from prePacks' size
    List<Pack> pack(Integer quantity, Product product) {

        List<PrePack> prePacks = product.getPrePacks();
        List<Pack> packs = new ArrayList<>();
        List<PrePack> orderedByQuantity = prePacks.stream()
                .sorted(Comparator.comparing(PrePack::getQuantity))
                .collect(Collectors.toList());
        int size = orderedByQuantity.size();

        if (size == 3) {

            PrePack prePack1 = orderedByQuantity.get(size - 1);
            PrePack prePack2 = orderedByQuantity.get(size - 2);
            PrePack prePack3 = orderedByQuantity.get(size - 3);

            for (int i = quantity / prePack1.getQuantity(); i > -1; i--) {
                int r1 = quantity - prePack1.getQuantity() * i;
                if (r1 == 0) {
                    packs.add(Pack.builder().prePack(prePack1).count(i).build());
                    return packs;
                } else {
                    for (int j = quantity / prePack2.getQuantity(); j > -1; j--) {
                        int r2 = r1 - prePack2.getQuantity() * j;
                        if (r2 == 0) {
                            packs.add(Pack.builder().prePack(prePack1).count(i).build());
                            packs.add(Pack.builder().prePack(prePack2).count(j).build());
                            return packs;
                        } else {
                            for (int k = quantity / prePack3.getQuantity(); k > -1; k--) {
                                int r3 = r2 - prePack3.getQuantity() * k;
                                if (r3 == 0) {
                                    packs.add(Pack.builder().prePack(prePack1).count(i).build());
                                    packs.add(Pack.builder().prePack(prePack2).count(j).build());
                                    packs.add(Pack.builder().prePack(prePack3).count(k).build());
                                    return packs;
                                }

                            }
                        }
                    }
                }
            }

        } else if (size == 2) {

            PrePack prePack1 = orderedByQuantity.get(size - 1);
            PrePack prePack2 = orderedByQuantity.get(size - 2);

            for (int i = quantity / prePack1.getQuantity(); i > -1; i--) {
                int r1 = quantity - prePack1.getQuantity() * i;
                if (r1 == 0) {
                    packs.add(Pack.builder().prePack(prePack1).count(i).build());
                    return packs;
                } else {
                    for (int j = quantity / prePack2.getQuantity(); j > -1; j--) {
                        int r2 = r1 - prePack2.getQuantity() * j;
                        if (r2 == 0) {
                            packs.add(Pack.builder().prePack(prePack1).count(i).build());
                            packs.add(Pack.builder().prePack(prePack2).count(j).build());
                            return packs;
                        }
                    }
                }
            }

        } else {
            throw new RuntimeException("there is not such pre-packaged item");
        }


        return packs;
    }
}
