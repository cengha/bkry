package com.cengha.bkry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BkryApplication {

	public static void main(String[] args) {
		SpringApplication.run(BkryApplication.class, args);
	}

}

