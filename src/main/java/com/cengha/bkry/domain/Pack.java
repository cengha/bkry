package com.cengha.bkry.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Pack {
    private int count;
    private PrePack prePack;
}
