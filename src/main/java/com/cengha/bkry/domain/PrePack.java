package com.cengha.bkry.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@Builder
public class PrePack {
    private Integer quantity;
    private BigDecimal price;
}
