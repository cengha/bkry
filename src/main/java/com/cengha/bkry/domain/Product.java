package com.cengha.bkry.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Product {
    private String code;
    private String name;
    private List<PrePack> prePacks;
}
