package com.cengha.bkry.controller;

import com.cengha.bkry.domain.Order;
import com.cengha.bkry.domain.dto.OrderPostRequest;
import com.cengha.bkry.service.OrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    public ResponseEntity<List<Order>> post(@RequestBody List<OrderPostRequest> request) {

        List<Order> orders = orderService.create(request);
        return ResponseEntity.ok(orders);
    }
}
