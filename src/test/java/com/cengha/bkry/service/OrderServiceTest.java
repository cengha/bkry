package com.cengha.bkry.service;

import com.cengha.bkry.dao.ProductDao;
import com.cengha.bkry.domain.Order;
import com.cengha.bkry.domain.Pack;
import com.cengha.bkry.domain.PrePack;
import com.cengha.bkry.domain.Product;
import com.cengha.bkry.domain.dto.OrderPostRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {


    @Mock
    private PackService packService;

    @Mock
    private ProductDao productDao;

    private OrderService orderService;

    @Before
    public void setUp() {
        orderService = new OrderService(productDao, packService);
    }

    @Test
    public void test_create() {

        List<PrePack> prePacks = new ArrayList<>();
        prePacks.add(PrePack.builder().quantity(3).price(BigDecimal.valueOf(6.99)).build());
        prePacks.add(PrePack.builder().quantity(5).price(BigDecimal.valueOf(8.99)).build());
        Product product = Product.builder().code("VS5").name("Vegemite Scroll").prePacks(prePacks).build();
        Optional<Product> productOptional = Optional.of(product);
        when(productDao.getByCode("VS5")).thenReturn(productOptional);

        PrePack prePack = PrePack.builder().price(BigDecimal.valueOf(8.99)).quantity(5).build();
        Pack pack = Pack.builder().count(2).prePack(prePack).build();
        when(packService.pack(10, productOptional.get())).thenReturn(Collections.singletonList(pack));

        List<String> items = Collections.singletonList("2 x 5 $8.99");
        String summary = "10 VS5 $17.98";
        List<Order> expected = Collections.singletonList(Order.builder().items(items).summary(summary).build());

        List<Order> actual = orderService.create(Collections.singletonList(new OrderPostRequest(10, "VS5")));

        assertEquals(expected, actual);

    }

}
