package com.cengha.bkry.controller;

import com.cengha.bkry.service.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderService;

    @Test
    public void test_orderPost() throws Exception {

        String request = "[" +
                "  {" +
                "    \"code\": \"VS5\"," +
                "    \"quantity\": 10" +
                "  }," +
                "{" +
                "    \"code\": \"MB11\"," +
                "    \"quantity\": 14" +
                "  }," +
                "{" +
                "    \"code\": \"CF\"," +
                "    \"quantity\": 13" +
                "  }" +
                "]";


        mockMvc.perform(post("/order")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(request))
                .andExpect(status().isOk());
    }

}
