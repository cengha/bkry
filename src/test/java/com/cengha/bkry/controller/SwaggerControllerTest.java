package com.cengha.bkry.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import springfox.documentation.swagger2.web.Swagger2Controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(Swagger2Controller.class)
public class SwaggerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void test_swagger_endpoint() throws Exception {

        mockMvc.perform(get("/swagger-ui.html#/"))
                .andExpect(status().isOk());
    }
}
