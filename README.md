# Weather Forecast


#### How to run?

1. Application uses **default** profile as default and runs on port **8090**.
2. Download project to your local computer, open terminal in root directory.
3. To build project type **./mvnw clean package** in terminal.
4. To run application you can type **./mvnw spring-boot:run**.
5. If you open **http://localhost:8090/swagger-ui.html#** from your browser, you will see **swagger** self api doc and be able test the API.
6. You may test API via swagger

#### Technology Stack

`Java 8, Spring Boot, Swagger, Lombok, JUnit, Mockito, Maven, Git`
